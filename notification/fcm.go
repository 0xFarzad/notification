package notification

import (
	"context"
	"errors"
	"log"
	"sync"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
	"google.golang.org/api/option"
)

var (
	// ErrEmptyToken you get this error if you choose any mode with token and not provided token.
	ErrEmptyToken = errors.New("token cannot be empty")
	// ErrEmptyTopic you get this error if you choose the send to topic mode but not provided topic.
	ErrEmptyTopic = errors.New("topic cannot be empty")
	// ErrEmptyCondition you get this error if you choose the send to condition mode but not provided condition.
	ErrEmptyCondition = errors.New("condition cannot be empty")
	// ErrEmptyMessages you get this error if you messages list is empty.
	ErrEmptyMessages = errors.New("messages cannot be empty")
	// ErrMessageOutOfRange you get this error if you  enter more than 100 messages.
	ErrMessageOutOfRange = errors.New("out of range. In every function call just can handle up to 100 messages")
)

var (
	fcmInstance *fcm
	once        sync.Once
	projectID   string
	filePath    string
)

type fcm struct {
	messages         []*messaging.Message
	message          *messaging.Message
	client           *messaging.Client
	multicastMessage *messaging.MulticastMessage
	notificationType notificationType
}

// FcmConfig is all you need to configure your fcm service.
type FcmConfig struct {
	ProjectID string
	FilePath  string
}

func initFcmInstance(cfg FcmConfig) {
	if fcmInstance == nil {
		once.Do(
			func() {
				ctx := context.Background()
				opt := option.WithCredentialsFile(cfg.FilePath)
				config := &firebase.Config{ProjectID: cfg.ProjectID}
				app, err := firebase.NewApp(ctx, config, opt)
				if err != nil {
					log.Fatalf("problem creating the fcm client; err: %s", err.Error())
				}
				client, err := app.Messaging(ctx)
				if err != nil {
					log.Fatalf("problem creating the fcm client; err: %s", err.Error())
				}
				fcmInstance = &fcm{client: client}
			})
	}
}

func getFcmInstance() *fcm {
	return fcmInstance
}

func (f *fcm) setMessage(msg interface{}) error {
	if f.notificationType == FcmNotificationTypeToMulticast {
		if multicastMessage, ok := msg.(*messaging.MulticastMessage); ok {
			f.multicastMessage = multicastMessage
			return nil
		}
	}
	if f.notificationType == FcmNotificationTypeToAll {
		if messages, ok := msg.([]*messaging.Message); ok {
			f.messages = messages
			return nil
		}
	}
	if f.notificationType == FcmNotificationTypeToDevice || f.notificationType == FcmNotificationTypeToTopic || f.notificationType == FcmNotificationTypeToCondition {
		if message, ok := msg.(*messaging.Message); ok {
			f.message = message
			return nil
		}
	}

	return ErrInvalidMessageType
}

func (f *fcm) setNotificationType(nType notificationType) error {
	if nType == FcmNotificationTypeToDevice || nType == FcmNotificationTypeToTopic || nType == FcmNotificationTypeToCondition || nType == FcmNotificationTypeToAll || nType == FcmNotificationTypeToMulticast {
		f.notificationType = nType
		return nil
	}
	return ErrInvalidTypeValue
}

func (f *fcm) send() error {
	return f.sendByType()
}

func (f *fcm) sendByType() error {
	switch f.notificationType {
	case FcmNotificationTypeToDevice:
		return f.sendToDevice()
	case FcmNotificationTypeToTopic:
		return f.sendToTopic()
	case FcmNotificationTypeToCondition:
		return f.sendToCondition()
	case FcmNotificationTypeToAll:
		return f.sendToAll()
	case FcmNotificationTypeToMulticast:
		return f.sendToMulticast()
	default:
		return nil
	}
}

func (f *fcm) simpleSend() error {
	_, err := f.client.Send(context.Background(), f.message)
	return err
}

func (f *fcm) sendToDevice() error {
	if f.message.Token == "" {
		return ErrEmptyToken
	}
	return f.simpleSend()
}

func (f *fcm) sendToTopic() error {
	if f.message.Topic == "" {
		return ErrEmptyTopic
	}
	return f.simpleSend()
}

func (f *fcm) sendToCondition() error {
	if f.message.Condition == "" {
		return ErrEmptyCondition
	}
	return f.simpleSend()
}

func (f *fcm) sendToMulticast() error {
	if len(f.multicastMessage.Tokens) == 0 {
		return ErrEmptyToken
	}
	_, err := f.client.SendMulticast(context.Background(), f.multicastMessage)

	return err
}

func (f *fcm) sendToAll() error {
	if len(f.messages) == 0 {
		return ErrEmptyMessages
	}
	if len(f.messages) > 100 {
		return ErrMessageOutOfRange
	}
	_, err := f.client.SendAll(context.Background(), f.messages)

	return err
}
