package notification

// Sender uses notification builder inside to use the abilities of builder.
type Sender struct {
	builder NotificationBuilder
}

// NewSender creates sender that can send message.
func NewSender(b NotificationBuilder, nType notificationType) (*Sender, error) {
	sender := &Sender{
		builder: b,
	}
	if err := sender.builder.setNotificationType(nType); err != nil {
		return nil, err
	}
	return sender, nil
}

// Send sends the message in your type.
func (d *Sender) Send(msg interface{}) error {
	if err := d.builder.setMessage(msg); err != nil {
		return err
	}
	return d.builder.send()
}
