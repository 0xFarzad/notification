package notification

import (
	"crypto/tls"
	"errors"
	"fmt"
	"html/template"
	"io"
	"log"

	"github.com/go-gomail/gomail"
)

var (
	// ErrEmptyHTMLPath you get this error if you choose the html mode but not provided path of html file.
	ErrEmptyHTMLPath = errors.New("HtmlPath cannot be empty")
	// ErrEmptyText you get this error if you choose the text mode but not provided any text.
	ErrEmptyText = errors.New("text cannot be empty")
)

var (
	emailInstance *email
)

type email struct {
	message          *gomail.Message
	dialer           *gomail.Dialer
	details          *EmailDetails
	notificationType notificationType
}

// EmailDetails uses as message input in setmessage func.
type EmailDetails struct {
	Receivers map[string]string
	Subject   string
	Text      string
	HTMLPath  string
}

// EmailConfig is all you need to configure your email service.
type EmailConfig struct {
	From     string
	Host     string
	Port     int
	Username string
	Password string
	Ssl      bool
	TLS      bool
}

func initEmailInstance(cfg EmailConfig) {
	if emailInstance == nil {
		once.Do(
			func() {
				message := gomail.NewMessage()
				message.SetHeader("From", cfg.From)
				dialer := gomail.NewDialer(cfg.Host, cfg.Port, cfg.Username, cfg.Password)
				dialer.TLSConfig = &tls.Config{InsecureSkipVerify: cfg.TLS}
				dialer.SSL = cfg.Ssl
				send, err := dialer.Dial()
				if err != nil {
					log.Fatalf("probelm creating the email client; err: %s", err.Error())
				}
				defer send.Close()

				emailInstance = &email{message: message, dialer: dialer}
			})
	}
}

func getEmailInstance() *email {
	return emailInstance
}

func (e *email) setMessage(msg interface{}) error {
	details, ok := msg.(*EmailDetails)
	if !ok {
		return ErrInvalidMessageType
	}
	e.details = details
	return nil
}

func (e *email) setNotificationType(nType notificationType) error {
	if nType == EmailNotificationTypeHTML || nType == EmailNotificationTypeText {
		e.notificationType = nType
		return nil
	}
	return ErrInvalidTypeValue
}

func (e *email) send() error {
	return e.sendByType()
}

func (e *email) sendByType() error {
	switch e.notificationType {
	case EmailNotificationTypeHTML:
		return e.sendHTML()
	case EmailNotificationTypeText:
		return e.sendText()
	default:
		return nil
	}
}

func (e *email) sendHTML() error {
	if e.details.HTMLPath == "" {
		return ErrEmptyHTMLPath
	}
	t, err := template.ParseFiles(e.details.HTMLPath)
	if err != nil {
		return err
	}
	e.message.SetHeader("Subject", e.details.Subject)
	var errors []error
	for name, email := range e.details.Receivers {
		e.message.SetHeader("To", email)
		e.message.AddAlternativeWriter("text/html", func(w io.Writer) error {
			return t.Execute(w, struct {
				Name string
			}{
				Name: name,
			})
		})
		if err := e.dialer.DialAndSend(e.message); err != nil {
			errors = append(errors, err)
		}
	}
	if len(errors) > 0 {
		err := fmt.Errorf("errors: %v", errors)
		return err
	}
	return nil
}

func (e *email) sendText() error {
	if e.details.Text == "" {
		return ErrEmptyText
	}
	e.message.SetHeader("Subject", e.details.Subject)
	e.message.SetBody("text/plain", e.details.Text)
	var errors []error
	for _, email := range e.details.Receivers {
		e.message.SetHeader("To", email)
		if err := e.dialer.DialAndSend(e.message); err != nil {
			errors = append(errors, err)
		}
	}
	if len(errors) > 0 {
		err := fmt.Errorf("errors: %v", errors)
		return err
	}
	return nil
}
