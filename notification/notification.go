package notification

import (
	"errors"
	"fmt"
)

var (
	// ErrInvalidTypeValue you get this error if you choose the bad notification type for your notification.
	ErrInvalidTypeValue = errors.New("invalid type value")
	// ErrInvalidMessageType you get this error if you choose the bad message type for your notification.
	ErrInvalidMessageType = errors.New("invalid message type")
)

type notificationType int

const (
	// FcmNotificationTypeToDevice uses to send push notification to one device with fcm token.
	FcmNotificationTypeToDevice notificationType = iota + 1
	
	// FcmNotificationTypeToTopic uses to send push notification to one topic with given topic name.
	FcmNotificationTypeToTopic
	
	// FcmNotificationTypeToCondition uses to send push notification to condition.
	FcmNotificationTypeToCondition

	// FcmNotificationTypeToMulticast uses to send push notification to one or more than one devices with fcm token.
	FcmNotificationTypeToMulticast

	// FcmNotificationTypeToAll uses to send push notifications to one device with fcm token.
	FcmNotificationTypeToAll

	// EmailNotificationTypeHTML uses to send email notification to one or more than one clients with html template.
	EmailNotificationTypeHTML

	// EmailNotificationTypeText uses to send email notification to one or more than one clients with text template.
	EmailNotificationTypeText
)

// NotificationBuilder is interface holds functionality of each notification type.
// setNotificationType give you ability to choose the type you want.
// setMessage sets the message of the notification.
// send is function that calls sendByType to sends notification based on type (ex: multicast, group, etc...).
type NotificationBuilder interface {
	setNotificationType(notificationType) error
	setMessage(interface{}) error
	send() error
	sendByType() error
}

// Config for init this module.
type Config struct {
	Email EmailConfig
	Fcm   FcmConfig
}

// New creates instance of each notification style based on provided config.
func New(o Config) error {
	if o.Email.Host == "" {
		return errors.New("Email, Host not provided")
	}
	if o.Email.Password == "" {
		return errors.New("Email, Password not provided")
	}
	if o.Email.Username == "" {
		return errors.New("Email, Username not provided")
	}
	if o.Email.From == "" {
		o.Email.From = fmt.Sprintf("%v %v", o.Email.Username, fmt.Sprintf("<no-reply@%v>", o.Email.Username))
	}
	initEmailInstance(o.Email)

	if o.Fcm.FilePath == "" {
		return errors.New("Fcm, File Path not provided")
	}
	if o.Fcm.ProjectID == "" {
		return errors.New("Fcm, Project Id not provided")
	}
	initFcmInstance(o.Fcm)
	return nil
}

// Get returns the notification builder based on notification type.
func Get(notificationType string) NotificationBuilder {
	if notificationType == "fcm" {
		return getFcmInstance()
	}

	if notificationType == "email" {
		return getEmailInstance()
	}

	return nil
}
